# README #

### What is this repository for? ###

* Fizzbuzz

### How do I run the code? ###

Run the following command: 
```
./gradlew build
```

This should have packaged the project into a jar. Navigate to build/libs and run
```
java -jar fizzbuzz.jar -max <number>
```
or
```
java -jar fizzbuzz.jar --maxNumber <number>
```

if you want to run it as a docker container after packaging the project run
```docker
docker build -t fizzbuzz .
docker run fizzbuzz <number>
```
