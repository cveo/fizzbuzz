package com.veo.fizzbuzz;

import com.veo.fizzbuzz.service.OptionsService;
import com.veo.fizzbuzz.service.StringCalculatorService;
import org.apache.commons.cli.*;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.Collection;

@SpringBootApplication
public class FizzbuzzApplication {

    @Bean
    public Options options(Collection<Option> allOptions) {
        Options options = new Options();
        allOptions.forEach(options::addOption);
        return options;
    }

    @Bean
    public CommandLineParser commandLineParser() {
        return new DefaultParser();
    }

    @Bean
    public HelpFormatter helpFormatter() {
        return new HelpFormatter();
    }

    @Bean
    public Option maxNumberOption() {
        Option maxNumber = new Option("max","maxNumber", true, "Maximum number to print out");
        maxNumber.setRequired(true);
        maxNumber.setType(Number.class);
        return  maxNumber;
    }
	public static void main(String[] args) {
		SpringApplication.run(FizzbuzzApplication.class, args);
	}

	@Bean
	public CommandLineRunner commandLineRunner(OptionsService optionsService, HelpFormatter helpFormatter, Options options, StringCalculatorService stringCalculatorService) {
		return args ->  {
			try {
				Long maxNumber = optionsService.getMaxNumber(args);
                System.out.println(stringCalculatorService.calculateFizzBuzz(maxNumber));
                System.out.println(maxNumber);
            } catch (ParseException e) {
                helpFormatter.printHelp("fizzbuzz",options);
			}

		};
	}
}
