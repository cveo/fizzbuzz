package com.veo.fizzbuzz.service;

public interface StringCalculatorService {

    String calculateFizzBuzz(Long maxNumber);
}
