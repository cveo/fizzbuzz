package com.veo.fizzbuzz.service;

import org.springframework.stereotype.Service;

import java.util.stream.Collectors;
import java.util.stream.LongStream;

@Service
public class StringCalculatorServiceManager implements StringCalculatorService {
    @Override
    public String calculateFizzBuzz(Long maxNumber) {
        return LongStream.rangeClosed(1, maxNumber).mapToObj(this::numberToFizzBuzz).collect(Collectors.joining(","));
    }

    private String numberToFizzBuzz(Long number) {
        String value = "";
        value += checkDivisor(number,3L," Fizz");
        value += checkDivisor(number, 5L, " Buzz");
        if(value.equals("")) {
            value += " " + number;
        }
        return value;
    }

    private String checkDivisor(Long number, Long divisor, String message) {
        String result = "";
        if(number % divisor == 0) {
            result = message;
        }
        return result;
    }
}
