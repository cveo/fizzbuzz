package com.veo.fizzbuzz.service;

import org.apache.commons.cli.*;
import org.springframework.stereotype.Service;

@Service
public class OptionsServiceManager implements OptionsService {

    private CommandLineParser parser;
    private Options options;
    private Option maxNumberOption;

    public OptionsServiceManager(CommandLineParser parser, Options options, Option maxNumberOption) {
        this.parser = parser;
        this.options = options;
        this.maxNumberOption = maxNumberOption;
    }

    @Override
    public Long getMaxNumber(String... args) throws ParseException{
            CommandLine commandLine = parser.parse(options, args);
            Object maxNumber = commandLine.getParsedOptionValue(maxNumberOption.getOpt());
            if(!(maxNumber instanceof Long)) {
                throw new ParseException("Max Number must be a whole number");
            }
            return (Long) maxNumber;
    }
}
