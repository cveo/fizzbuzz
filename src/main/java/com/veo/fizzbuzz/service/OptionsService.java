package com.veo.fizzbuzz.service;

import org.apache.commons.cli.ParseException;

public interface OptionsService {

    Long getMaxNumber(String ... args) throws ParseException;
}
