package com.veo.fizzbuzz.test;

import com.veo.fizzbuzz.service.OptionsServiceManager;
import org.apache.commons.cli.*;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class OptionsServiceManagerTests {

    private CommandLineParser commandLineParser;
    private Options options;
    private Option maxNumberOption;

    private OptionsServiceManager optionsServiceManager;

    @Before
    public void setupManager() {
        commandLineParser = new DefaultParser();
        maxNumberOption = new Option("max", "maxNumber", true, "Some description");
        maxNumberOption.setRequired(true);
        maxNumberOption.setType(Number.class);
        options = new Options();
        options.addOption(maxNumberOption);
        optionsServiceManager = new OptionsServiceManager(commandLineParser,options,maxNumberOption);
    }

    @Test(expected = ParseException.class)
    public void doubleMaxValueThrowsError() throws ParseException {
        String[] args = new String[] {"-max","12.2"};
        optionsServiceManager.getMaxNumber(args);
    }

    @Test
    public void returnsLongMaxValue() throws ParseException {
        Long maxValue = 20L;
        String[] args = new String[] {"-max", String.valueOf(maxValue)};
        Long result = optionsServiceManager.getMaxNumber(args);
        assertEquals(maxValue,result);
    }

}
