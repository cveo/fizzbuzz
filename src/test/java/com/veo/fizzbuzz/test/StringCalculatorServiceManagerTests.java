package com.veo.fizzbuzz.test;

import com.veo.fizzbuzz.service.StringCalculatorServiceManager;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class StringCalculatorServiceManagerTests {

    private StringCalculatorServiceManager stringCalculatorServiceManager = new StringCalculatorServiceManager();

    @Test
    public void maxNumberMultipleOfThreeReturnsFizz() {
        String result = stringCalculatorServiceManager.calculateFizzBuzz(3L);
        assertTrue(result.endsWith("Fizz"));
    }

    @Test
    public void maxNumberMultipleOfFiveReturnsBuzz() {
        String result = stringCalculatorServiceManager.calculateFizzBuzz(5L);
        assertTrue(result.endsWith("Buzz"));
    }

    @Test
    public void maxNumberMultipleOfThreeAndFiveReturnsFizzBuzz() {
        String result = stringCalculatorServiceManager.calculateFizzBuzz(3L * 5L);
        assertTrue(result.endsWith("Fizz Buzz"));
    }

    @Test
    public void maxNumberNotMultipleOfThreeOrFiveReturnsMaxNumber() {
        Long maxNumber = 11L;
        String result = stringCalculatorServiceManager.calculateFizzBuzz(maxNumber);
        assertTrue(result.endsWith(String.valueOf(maxNumber)));
    }
}
