FROM openjdk:8u131-jre-alpine

MAINTAINER carlo.a.veo@gmail.com

COPY build/libs/fizzbuzz.jar /

ENTRYPOINT ["java","-jar", "fizzbuzz.jar","-max"]